package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class PasswordIsEmptyException extends AbstractException {

    public PasswordIsEmptyException() {
        super("Error! Password is empty...");
    }

}
