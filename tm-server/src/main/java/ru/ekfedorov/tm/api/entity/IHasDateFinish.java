package ru.ekfedorov.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasDateFinish {

    Date getDateFinish();

    void setDateFinish(@NotNull Date dateFinish);

}
