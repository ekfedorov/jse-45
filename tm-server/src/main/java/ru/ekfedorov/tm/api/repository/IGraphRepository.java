package ru.ekfedorov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.model.AbstractGraphEntity;

import javax.persistence.TypedQuery;
import java.util.Optional;


public interface IGraphRepository<E extends AbstractGraphEntity> {

    void add(@NotNull E entity);

    Optional<E> getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
